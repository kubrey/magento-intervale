<?php

/**
 * <b>Основной модуль intervale</b><br>
 * Используется редирект после чекаута на страницу платежки<br>
 * ссылка для редиректа формируется в getOrderPlaceRedirectUrl<br>
 * Дальше платежка посылает запрос на проверку доступности платежа<br>
 * на /intervale/index/available<br>
 * Вызывается isPaymentAvailable , которая через Модель Availability<br>
 * проверяет доступность платежа по пришедшим данным(описаны в модели Availability)<br>
 * и возвращает xml<br>
 * После заполнения кастомером формы на сайте платежки<br>
 * От нее приходит запрос на регистрацию завершения платежа(как успешного, так и нет)<br>
 * на /intervale/index/register, где вызывается->attemptToRegisterPaymen,<br>
 * которая, используя модель Registration,<br>
 * проверяет поступившые данные и либо делает заказу complete либо cancel<br>
 * в зависимости от поступившего статуса.<br>
 * и возвращает xml с ответом, удалось ли обработать окончания заказа<br>
 *
 * @property DomDocument $_xml
 * @property Mage_Sales_Model_Order $_order
 * @author kubrey
 */
class Kubrey_Intervale_Model_Intervale extends Mage_Payment_Model_Method_Abstract
{

    protected $_code = 'intervale';
    protected $_xml;
    protected $_order = null;
    protected $_orderId;
    //
    protected $_config = null;
    protected $_avail = null;
    protected $_reg = null;
    //
    protected $_helper = null;
    //options payment start
    private $_lang = 'en';
    private $_pageId;
    private $_merchId;
    private $_backUrlS;
    private $_backUrlF;
    protected $_options = array();
    protected $_settings = array(
        'lang' => 'lang',
        'pageId' => 'page_id',
        'merchId' => 'merch_id',
        'backUrlS' => 'back_url_s',
        'backUrlF' => 'back_url_f',
    );
    //для вывода описания платежного метода
    protected $_formBlockType = 'intervale/paymentInfo';

    /**
     *
     * @return Kubrey_Intervale_Model_Config
     */
    protected function getConfig() {
        if (!$this->_config) {
            $this->_config = Mage::getModel('intervale/config');
        }
        return $this->_config;
    }

    /**
     *
     * @return Kubrey_Intervale_Model_Availability
     */
    protected function getAvailModel() {
        if (!$this->_avail) {
            $this->_avail = Mage::getModel('intervale/availability');
        }
        return $this->_avail;
    }

    /**
     *
     * @return Kubrey_Intervale_Model_Registration
     */
    protected function getRegisterModel() {
        if (!$this->_reg) {
            $this->_reg = Mage::getModel('intervale/registration');
        }
        return $this->_reg;
    }

    /**
     * @return \Kubrey_Intervale_Helper_Data
     */
    public function getHelper() {
        if (!$this->_helper) {
            $this->_helper = Mage::helper('intervale');
        }
        return $this->_helper;
    }

    /**
     *
     * @param type $lang
     * @return \Kubrey_Intervale_Model_Intervale
     */
    protected function setLang($lang) {
        $this->_lang = $lang;
        return $this;
    }

    /**
     *
     * @param type $pageId
     * @return \Kubrey_Intervale_Model_Intervale
     */
    protected function setPageid($pageId) {
        $this->_pageId = $pageId;
        return $this;
    }

    /**
     *
     * @param type $merchId
     * @return \Kubrey_Intervale_Model_Intervale
     */
    protected function setMerchId($merchId) {
        $this->_merchId = $merchId;
        return $this;
    }

    /**
     *
     * @param type $url
     * @return \Kubrey_Intervale_Model_Intervale
     */
    protected function setBackUrlS($url) {
        $this->_backUrlS = $url;
        return $this;
    }

    /**
     *
     * @param string $url
     * @return \Kubrey_Intervale_Model_Intervale
     */
    protected function setBackUrlF($url) {
        $this->_backUrlF = $url;
        return $this;
    }

    protected function setOption($name, $val) {
        $this->_options[$name] = $val;
        return $this;
    }

    /**
     *
     * @param string $key
     * @param string $val
     * @return boolean
     */
    protected function check4option($key, $val) {
        if (strpos($key, "o.") === 0) {
            $this->setOption(str_replace("o.", "", $key), $val);
            return true;
        } elseif (strpos($key, "o_") === 0) {
            //магенто преобразовывает GET-параметры с ключом o.amount к виду o_amount - поэтому добавлена такая проверка
            $this->setOption(str_replace("o_", "", $key), $val);
            return true;
        }

        return false;
    }

    /**
     * Ссылка для редиректа на страницу платежки после чекаута
     * @return string
     */
    public function getOrderPlaceRedirectUrl() {
        return Mage::getUrl('intervale/gateway/redirect', array('_secure' => true));
    }

    /**
     * Ссылка на страницу платежки со всем нудными аргументами
     * @return string
     */
    public function getPaymentUrl() {
        $this->initiatePayment();
        $url = $this->getConfig()->getPaymentStartUrl();
        $params['lang'] = $this->_lang;
        $params['page_id'] = $this->_pageId;
        $params['merch_id'] = $this->_merchId;
        $params['back_url_s'] = $this->_backUrlS;
        $params['back_url_f'] = $this->_backUrlF;

        if ($this->_options) {
            foreach ($this->_options as $f => $v) {
                if (!$v) {
                    continue;
                }
                $params['o.' . $f] = $v;
            }
        }

        $url .= "?" . http_build_query($params);
        $this->getHelper()->log("initiate payment url: " . $url, Zend_Log::DEBUG);
        return $url;
    }

    /**
     *
     * @param array $data
     * @return string
     */
    public function isPaymentAvailable($data) {
        return $this->getAvailModel()->handleRequest($data);
    }

    /**
     *
     * @param array $data
     * @return string
     */
    public function attemptToRegisterPayment($data) {
        return $this->getRegisterModel()->handleRequest($data);
    }

    /**
     * формирование редиректа на страницу оплаты после заполнения чекаута<br>
     * В опции записываем id заказа и данные по оплате<br>
     * Эти данные должны прийти в  isPaymentAvailable и там свериться с данными из базы<br>
     * Передается id заказа для связи. Чтобы был id заказа в сесии, после checkout нужен редирект на нашу страницу, а оттуда уже по этой ссылке<br>
     * Чтобы передавать id заказа, нужно сначала сделат редирект на страницу в магазине, а оттуда на платежку
     * @see isPaymentAvailable
     * @throws Mage_Checkout_Exception
     */
    public function initiatePayment() {
        $session = Mage::getSingleton('checkout/session');
        $quoteId = $session->getQuoteId();

        $order = Mage::getModel('sales/order')->loadByIncrementId($session->getLastRealOrderId());
        if (!$order->getId()) {
            throw new Mage_Checkout_Exception("Error occurred");
        }

        $amount = $order->getData('grand_total');

        $this->setBackUrlS($this->getConfig()->getSuccessUrl());
        $this->setBackUrlF($this->getConfig()->getFailureUrl());
        $this->setOption('quote_id', $quoteId);
        $this->setOption('order_num', $session->getLastRealOrderId());
        $this->setOption('order_id', $order->getId());
        $this->setOption('company', "Polsky.TV");
        $this->setOption('amount', (int)round($amount * 100));
//        $this->setOption('currency_code', $this->getConfig()->getCurrencyByCode($curr));
        $this->setMerchId($this->getConfigData('merch_id'));
        $this->setPageid($this->getConfigData('page_id'));
    }

    /**
     *
     * @return array
     */
    protected function getOptions() {
        return $this->_options;
    }

    /**
     *
     * @param array $data
     * @return boolean
     */
    protected function handleParams($data) {

        $settings = $this->getSettings();
        if (!$data || !is_array($data)) {
            return false;
        }
        //перебор поступивших GET параметров
        foreach ($data as $field => $val) {
            $key = array_search($field, $settings);
            if (!$key) {
                //если ключ не найден - возможно это опция o.*
                $this->check4option($field, $val);
                continue;
            }
            $setter = 'set' . ucfirst($key);
            $this->{$setter}($val);
        }
        return true;
    }

    /**
     *
     * @param string $param
     * @return mixed|null null - если не указана
     */
    protected function getOption($param) {
        $options = $this->getOptions();
        return (isset($options[$param]) ? $options[$param] : null);
    }

    /**
     *
     * @return array
     */
    protected function getSettingsValues() {
        $settings = array();
        foreach ($this->getSettings() as $key => $val) {
            $settings[$val] = $this->{"_" . $key};
        }
        return $settings;
    }

    protected function getSettings() {
        return $this->_settings;
    }

}