<?php

/**
 * Класс с настройками Intervale
 *
 * @author kubrey
 */
class Kubrey_Intervale_Model_Config extends Mage_Payment_Model_Config {

    private $merchantId;
    protected static $_currencyCodes = array(
        'USD' => 840,
        'EUR' => 978,
        'GEL' => 981
    );

    public function getMerchantId() {
        return $this->merchantId = 'QWERTYUIOPASDFGHJKLZXCVBNM123456';
    }

    public function getPageId() {
        return 'QWERTYUIOPASDFGHJKLZXCVBNM123456';
    }

    public function getAccountId() {
        return ';QWERTYUIOPASDFGHJKLZXCVBNM123456' //USD
    }

    public function getSuccessUrl() {
        return Mage::getUrl('checkout/onepage/success');
    }

    public function getFailureUrl() {
        return Mage::getUrl('checkout/onepage/failure');
    }

    /**
     * 
     * @return string
     */
    public function getPaymentStartUrl() {
        return 'http://pps.intervale.ru/payment/start.wsm';
    }

    /**
     * 
     * @return string
     */
    public function getCheckPaymentAvailUrl() {
        $url = Mage::getUrl('intervale/index/available', array('_secure' => true));
        return str_replace('http://', 'https://', $url);
    }

    /**
     * 
     * @return string
     */
    public function getRegisterPaymentUrl() {
        $url = Mage::getUrl('intervale/index/register', array('_secure' => true));
        return str_replace('http://', 'https://', $url);
    }

    /**
     * По 3-буквенному коду страны возвращает цифровой код валюты
     * @param string $code
     * @return int|boolean
     */
    public function getCurrencyByCode($code) {
        if (array_key_exists($code, self::$_currencyCodes)) {
            return self::$_currencyCodes[$code];
        } else {
            return false;
        }
    }

}
