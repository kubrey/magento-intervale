<?php

/**
 * Description of Registration
 *
 * @author kubrey
 */
class Kubrey_Intervale_Model_Registration extends Kubrey_Intervale_Model_Intervale {

    private $_resultCode; // 1|2
    private $_resultDesc;
    //RegisterPayment
    private $_merchantTrx;
    private $_trxId;
    private $_resultCodeReq;
    private $_amount;
    private $_merchId;
    private $_accountId;
    private $_signature;
    private $_maskedPan;
    private $_cardholder;
    protected $_options = array();
    protected $_settings = array(
        'resultCodeReq' => 'result_code',
        'amount' => 'amount',
        'accountId' => 'account_id',
        'merchantTrx' => 'merchant_trx',
        'trxId' => 'trx_id',
        'signature' => 'signature',
        'lang' => 'lang',
        'ts' => 'ts',
        'merchId' => 'merch_id',
        'maskedPan' => 'p_maskedPan',
        'cardholder' => 'p_cardholder'
    );

    /**
     * 
     * @param array $data массив с get-данными от платежки, по которым определяется, регить платеж или нет
     * @return string|boolean
     */
    public function handleRequest($data) {
        try {
            if (!$data) {
                throw new Exception("No params set");
            }
            //обрабатываем параметры
            $this->handleParams($data);
            //валидируем
            $this->check();
            //обрабатываем платеж - успешный или нет
            $this->registerPayment();
            $this->_resultCode = 1;
            $this->_resultDesc = 'OK';
        } catch (Exception $e) {
            Mage::logException($e);
            $this->getHelper()->log($e->getMessage(), Zend_Log::ERR);
            $this->_resultCode = 2;
            $this->_resultDesc = $e->getMessage();
        }
        $request = print_r($this->getSettingsValues(), 1);
        $response[] = $this->_resultCode;
        $response[] = $this->_resultDesc;
        $this->getHelper()->log("register request: " . $request . "\n register response: " . print_r($response, 1), Zend_Log::DEBUG);
        return $this->createXml();
    }

    /**
     * 
     * @return boolean|string
     */
    protected function createXml() {
        $this->_xml = new DomDocument('1.0', 'utf-8');
        $response = $this->_xml->appendChild($this->_xml->createElement('register-payment-response'));
        //result block
        $result = $response->appendChild($this->_xml->createElement('result'));
        $code = $result->appendChild($this->_xml->createElement('code'));
        $code->appendChild($this->_xml->createTextNode($this->_resultCode));
        $desc = $result->appendChild($this->_xml->createElement('desc'));
        $desc->appendChild($this->_xml->createTextNode($this->_resultDesc));
        return $this->_xml->saveXML();
    }

    /**
     * 
     * @param type $lang
     * @return \Kubrey_Intervale_Model_Registration
     */
    protected function setLang($lang) {
        $this->_lang = $lang;
        return $this;
    }

    /**
     * 
     * @param type $merchId
     * @return \Kubrey_Intervale_Model_Registration
     */
    protected function setMerchId($merchId) {
        $this->_merchId = $merchId;
        return $this;
    }

    /**
     * 
     * @param string $cc номер кредитки вида 900000xxxxxxxxx0001
     * @return \Kubrey_Intervale_Model_Registration
     */
    protected function setMaskedPan($cc) {
        $this->_maskedPan = $cc;
        return $this;
    }

    /**
     * 
     * @param string $cardHolder владелец кредитки
     * @return \Kubrey_Intervale_Model_Registration
     */
    protected function setCardholder($cardHolder) {
        $this->_cardholder = $cardHolder;
        return $this;
    }

    /**
     * 
     * @param string $trx
     * @return \Kubrey_Intervale_Model_Registration
     */
    protected function setMerchantTrx($trx) {
        $this->_merchantTrx = $trx;
        return $this;
    }

    /**
     * 
     * @param string $trx
     * @return \Kubrey_Intervale_Model_Registration
     */
    protected function setTrxId($trx) {
        $this->_trxId = $trx;
        return $this;
    }

    /**
     * 
     * @param int $code
     * @return \Kubrey_Intervale_Model_Registration
     */
    protected function setResultCodeReq($code) {
        $this->_resultCodeReq = $code;
        return $this;
    }

    /**
     * 
     * @param float $amount
     * @return \Kubrey_Intervale_Model_Registration
     */
    protected function setAmount($amount) {
        $this->_amount = $amount;
        return $this;
    }

    /**
     * 
     * @param string $acc
     * @return \Kubrey_Intervale_Model_Registration
     */
    protected function setAccountId($acc) {
        $this->_accountId = $acc;
        return $this;
    }

    /**
     * 
     * @param string $sign
     * @return \Kubrey_Intervale_Model_Registration
     */
    protected function setSignature($sign) {
        $this->_signature = $sign;
        return $this;
    }

    /**
     * 
     * @param string $ts timestamp
     * @return \Kubrey_Intervale_Model_Registration
     */
    protected function setTs($ts) {
        $this->_ts = $ts;
        return $this;
    }

    /**
     * 
     * @throws Exception
     * @return boolean
     */
    protected function check() {
        $merch = $this->_merchId;
        if ($merch != $this->getConfigData('merch_id')) {
            throw new Exception("merch_id " . $merch . " is not equal to our merch");
        }
        $transId = $this->_trxId;
        if (!$transId) {
            throw new Exception("trx_id is not set");
        }
        $orderId = $this->getOption("order_id");
        if (!$orderId) {
            throw new Exception("Failed to get order id for trx_id " . $this->_trxId);
        }
        $orderModel = Mage::getModel("sales/order");
        $order = $orderModel->load($orderId);
        if (!$order->getId()) {
            throw new Exception("Failed to get order by order " . $orderId);
        }
        $this->_order = $order;
        $amount = $this->_order->getData('grand_total');
        $amountInt = (int) round($amount * 100); //19.95=>1995
        $currency = $this->_order->getData('order_currency_code');
        //сравниваем account_id валюты заказа
        if ($this->_accountId != $this->getConfigData('account_id_' . strtolower($currency))) {
            throw new Exception("Received account is not equal to our account_id for " . $currency);
        }

        //сравниваем сумму в заказе с суммой, которая пришла от платежки
        if ($amountInt != $this->_amount && $this->_resultCode == 1) {
            throw new Exception("Received amount value is not equal to the ordered value");
        }

        $this->_orderId = $order->getId();
        return true;
    }

    /**
     * 
     * @throws Exception
     */
    protected function registerPayment() {
        $data = $this->getSettings();
        $this->getHelper()->log(__METHOD__ . $this->_resultCodeReq . "\n" . serialize($data));
        try {
            if ($this->_resultCodeReq == 1) {
                //payed ->ok
                $this->registerPaymentCapture();
            } elseif ($this->_resultCodeReq == 2) {
                //not payed -> cancel
                $this->registerPaymentCanceled();
            }
        } catch (Exception $e) {
            Mage::logException($e);
            $this->getHelper()->log($e->getMessage(), Zend_Log::ERR);
            throw new Exception("Error ocurried while handling payment result");
        }
    }

    /**
     * 
     * @return \Kubrey_Intervale_Model_Registration
     */
    private function registerPaymentCapture() {
        $data = $this->getSettings();
        $encryptedData = Mage::helper('core')->encrypt(serialize($data));

        $payment = $this->_order->getPayment();
        $idTrans = $this->_trxId;
        $cclast4 = substr($this->_maskedPan, -4);
        $payment->setTransactionId($idTrans)
                ->setParentTransactionId($this->_order->getData('customer_email'))
                ->setShouldCloseParentTransaction('ok')
                ->setIsTransactionClosed(0)
                ->registerCaptureNotification($this->_order->getGrandTotal());
        $payment->setData('additional_data', $encryptedData);
        $payment->setData('cc_last4', $cclast4);
        $payment->setData('cc_owner', $this->_cardholder);
        $payment->setData('cc_number_enc', Mage::helper('core')->encrypt($this->_maskedPan));

        $this->_order->setData('state', Mage_Sales_Model_Order::STATE_COMPLETE);
        $this->_order->setStatus(Mage_Sales_Model_Order::STATE_COMPLETE);
        $this->_order->save();

        if ($invoice = $payment->getCreatedInvoice()) {
            $this->_order->addStatusHistoryComment(
                            Mage::helper('backoffice')->__('Notified customer about invoice #%s.', $invoice->getIncrementId())
                    )
                    ->setIsCustomerNotified(true)
                    ->save();
        }
        return $this;
    }

    /**
     * 
     * @return \Kubrey_Intervale_Model_Registration
     */
    private function registerPaymentCanceled() {
        $this->getHelper()->log($this->_orderId . " status=>canceled");
        $payment = $this->_order->getPayment();
        if ($this->_order->canCancel()) {
            $this->_order->cancel();
            $payment->setData('additional_data', serialize($this->getSettings()));
            $this->_order->addStatusToHistory(Mage_Sales_Model_Order::STATE_CANCELED, "");
            $this->_order->save();
        } else {
            $this->getHelper()->log($this->_order->getId() . " failed to be canceled", Zend_Log::WARN);
        }
        $this->_order->save();

        return $this;
    }

}
