<?php

/**
 * Description of Availability
 *
 * @author kubrey
 */
class Kubrey_Intervale_Model_Availability extends Kubrey_Intervale_Model_Intervale {

    //данные ответа
    private $_resultCode; // 1|2
    private $_resultDesc;
    private $_merchantTrx;
    private $_purchase = array(
        'shortPurchaseDesc' => null,
        'longPurchaseDesc' => null,
        'account-amount' => array(
            'id' => null,
            'amount' => null,
            'currency' => null,
            'exponent' => null
        )
    );
    //данные запроса
    //CheckPaymentAvailRequest
    private $_trxId; //id транзакции в платежке
    private $_merchId; //id магазина в админке платежки
    private $_ts; //yyyyMMdd HH:mm:ss
    protected $_options = array();
    private $_lang; //ISO-3166
    protected $_settings = array(
        'trxId' => 'trx_id',
        'ts' => 'ts',
        'lang' => 'lang',
        'merchId' => 'merch_id'
    );

    /**
     * 
     * @param array $data массив с get-данными от платежки, по которым определяется, разрешать платеж или нет
     * @return boolean|string
     */
    public function handleRequest($data) {
        try {
            if (!$data) {
                throw new Exception("No params set");
            }
            $this->handleParams($data);
            $this->check();
            $this->setResponse();
        } catch (Exception $ex) {
            Mage::logException($ex);
            $this->getHelper()->log($ex->getMessage(), Zend_Log::ERR);
            $this->_resultCode = 2;
            $this->_resultDesc = $ex->getMessage(); //'Unable to accept this payment'; 
        }
        $request = print_r($this->getSettingsValues(), 1);
        $response[] = $this->_resultCode;
        $response[] = $this->_resultDesc;
        $response[] = $this->_merchantTrx;
        $response[] = serialize($this->_purchase);

        $xml = $this->createXml();
        $this->getHelper()->log("availability request: " . $request . "\n availability  response: " . print_r($response, 1) . "\navailable xml:\n " . $xml, Zend_Log::DEBUG);
        return $xml;
    }

    protected function setResponse() {
        $this->_resultCode = 1;
        $this->_resultDesc = 'OK';
        $this->_merchantTrx = $this->_trxId;
        $orderNum = $this->_order->getData('increment_id');
        $curr = $this->_order->getData('order_currency_code');
        if (!$this->getConfigData('account_id_' . strtolower($curr))) {
            throw new Exception("No account id for " . $curr);
        }
        $descLong = $this->_order->getData('customer_firstname') . ' ' . $this->_order->getData('customer_lastname') . ' #' . $orderNum;
        $this->_purchase['shortPurchaseDesc'] = '#' . $orderNum;
        $this->_purchase['longPurchaseDesc'] = $descLong;
        $this->_purchase['account-amount']['id'] = $this->getConfigData('account_id_' . strtolower($curr));
        $this->_purchase['account-amount']['amount'] = (int) round($this->_order->getData('grand_total') * 100);
        $this->_purchase['account-amount']['currency'] = $this->getConfig()->getCurrencyByCode($curr);
        $this->_purchase['account-amount']['exponent'] = 2; //количество цифр после запятой в amount
    }

    /**
     * 
     * @return boolean|string
     */
    protected function createXml() {
        $this->_xml = new DomDocument('1.0', 'utf-8');
        $response = $this->_xml->appendChild($this->_xml->createElement('payment-avail-response'));
        //result block
        $result = $response->appendChild($this->_xml->createElement('result'));
        $code = $result->appendChild($this->_xml->createElement('code'));
        $code->appendChild($this->_xml->createTextNode($this->_resultCode));
        $desc = $result->appendChild($this->_xml->createElement('desc'));
        $desc->appendChild($this->_xml->createTextNode($this->_resultDesc));
        //
        if ($this->_resultCode == 2) {
            return $this->_xml->saveXML();
        }
        //
        $merchTrx = $response->appendChild($this->_xml->createElement('merchant-trx'));
        $merchTrx->appendChild($this->_xml->createTextNode($this->_merchantTrx));
        //purchase block
        $purchase = $response->appendChild($this->_xml->createElement('purchase'));
        $shortDesc = $purchase->appendChild($this->_xml->createElement('shortDesc'));
        $shortDesc->appendChild($this->_xml->createTextNode($this->_purchase['shortPurchaseDesc']));
        $longDesc = $purchase->appendChild($this->_xml->createElement('longDesc'));
        $longDesc->appendChild($this->_xml->createTextNode($this->_purchase['longPurchaseDesc']));
        //account-amount
        $accAmount = $purchase->appendChild($this->_xml->createElement('account-amount'));
        $accId = $accAmount->appendChild($this->_xml->createElement('id'));
        $accId->appendChild($this->_xml->createTextNode($this->_purchase['account-amount']['id']));
        $accAm = $accAmount->appendChild($this->_xml->createElement('amount'));
        $accAm->appendChild($this->_xml->createTextNode($this->_purchase['account-amount']['amount']));
        $accCurrency = $accAmount->appendChild($this->_xml->createElement('currency'));
        $accCurrency->appendChild($this->_xml->createTextNode($this->_purchase['account-amount']['currency']));
        $accExp = $accAmount->appendChild($this->_xml->createElement('exponent'));
        $accExp->appendChild($this->_xml->createTextNode($this->_purchase['account-amount']['exponent']));
        return $this->_xml->saveXML();
    }

    /**
     * 
     * @param type $lang
     * @return \Kubrey_Intervale_Model_Availability
     */
    protected function setLang($lang) {
        $this->_lang = $lang;
        return $this;
    }

    /**
     * 
     * @param type $merchId
     * @return \Kubrey_Intervale_Model_Availability
     */
    protected function setMerchId($merchId) {
        $this->_merchId = $merchId;
        return $this;
    }

    /**
     * 
     * @param string $id
     * @return \Kubrey_Intervale_Model_Availability
     */
    protected function setTrxId($id) {
        $this->_trxId = $id;
        return $this;
    }

    /**
     * 
     * @param string $ts timestamp
     * @return \Kubrey_Intervale_Model_Availability
     */
    protected function setTs($ts) {
        $this->_ts = $ts;
        return $this;
    }

    /**
     * 
     * @throws Exception
     * @return boolean
     */
    protected function check() {
        $merch = $this->_merchId;
        if ($merch != $this->getConfigData('merch_id')) {
            throw new Exception("merch_id " . $merch . " is not equal to our merch");
        }
        $transId = $this->_trxId;
        if (!$transId) {
            throw new Exception("trx_id is not set");
        }
        $orderId = $this->getOption("order_id");

        if (!$orderId) {
            throw new Exception("Failed to get order id for trx_id " . $this->_trxId);
        }
        $orderModel = Mage::getModel("sales/order");
        $order = $orderModel->load($orderId);
        if (!$order->getId()) {
            throw new Exception("Failed to get order by order " . $orderId);
        }
        $this->_order = $order;
        $this->_orderId = $order->getId();
        return true;
    }

}
