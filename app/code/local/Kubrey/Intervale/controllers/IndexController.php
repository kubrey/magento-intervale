<?php

/**
 * Description of IndexController
 *
 * @author kubrey
 */
class Kubrey_Intervale_IndexController extends Mage_Core_Controller_Front_Action
{

    protected $_model;

    /**
     *
     * @return \Kubrey_Intervale_Model_Intervale
     */
    protected function getModel() {
        if (!$this->_model) {
            $this->_model = Mage::getModel('intervale/intervale');
        }
        return $this->_model;
    }

    /**
     *
     * @return \Kubrey_Intervale_Model_Config
     */
    protected function getConfig() {
        if (!$this->_model) {
            $this->_model = Mage::getModel('intervale/config');
        }
        return $this->_model;
    }

    /**
     *
     */
    public function availableAction() {
        $this->getModel()->getHelper()->log("available action: " . print_r($this->getRequest()->getParams(), 1));
        $this->getResponse()->setHeader('Content-type', 'application/xml', true);
        $this->getResponse()->setHeader('HTTP/1.1', '200 OK', true);
        echo $this->getModel()->isPaymentAvailable($this->getRequest()->getParams());
    }

    /**
     *
     */
    public function registerAction() {
        $this->getModel()->getHelper()->log("register action " . print_r($this->getRequest()->getParams(), 1));
        $this->getResponse()->setHeader('Content-type', 'application/xml', true);
        $this->getResponse()->setHeader('HTTP/1.1', '200 OK', true);
        echo $this->getModel()->attemptToRegisterPayment($this->getRequest()->getParams());
    }

   
    


}
