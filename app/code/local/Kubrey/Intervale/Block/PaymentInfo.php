<?php

/**
 * Выводится в описании платежного метода в checkout
 *
 * @author kubrey
 */
class Kubrey_Intervale_Block_PaymentInfo extends Mage_Core_Block_Template{

    protected function _construct() {
        parent::_construct();
        $this->setTemplate('intervale/payment_info.phtml');
        
    }
    
    public function getMethodCode(){
        return $this->getModel()->getCode();
    }
    
    /**
     * 
     * @return Kubrey_Intervale_Model_Intervale
     */
    protected function getModel(){
        return Mage::getModel('intervale/intervale');
    }

}