<?php

/**
 * Description of Kubrey_Intervale_Helper_Data
 *
 * @author kubrey
 */
class Kubrey_Intervale_Helper_Data extends Mage_Core_Helper_Abstract {

    private $_logLevel = 7; //уровень, выше которого логи не записываются,соответствует Zend_Log::... от 0 до 7, 0 - наибольшая важность

    /**
     * 
     * @param string $mail
     * @return boolean
     */
    public function shouldBeOutputed($mail = null) {
        return true;
    }

    /**
     * 
     * @param string $msg
     * @param int $level
     */
    public function log($msg, $level = Zend_Log::DEBUG) {
        $logFile = "intervale" . date('Ym') . '.log';
        if ($this->_logLevel >= $level) {
            Mage::log($msg, $level, $logFile);
        }
    }

}
